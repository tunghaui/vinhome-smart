$('.background-image').slick({
  dots: true,
  infinite: true,
  autoplay: true, 
  speed: 600,
  prevArrow: '<i class="fa fa-angle-left prevArrow" aria-hidden="true"></i>',
  nextArrow: '<i class="fa fa-angle-right nextArrow" aria-hidden="true"></i>',
  slidesToShow: 1,
  // adaptiveHeight: true
});

$

$('.search button').click(function(e) {
	e.preventDefault();
	if($('.search-box').is(':hidden')){
		$('.search-box').show();
	}
	else{
		$('.search-box').hide();
	}
});

$('.m-btn').click(function(e){
  e.preventDefault();
  if($(window).width() < 991) {
      $('.nav-bar-menu').addClass('active');
  };
  $('.m-nav-over').css({
    display: 'block'
  });
});


$('.m-nav-over').click(function(e){
  e.preventDefault();
  $(this).css({
    display: ''    
  });
  $('.nav-bar-menu').removeClass('active');
})

$('.cl-btn').click(function(e) {
  e.preventDefault();
  $('.nav-bar-menu').removeClass('active');
  $('.m-nav-over').css({
    display: ''    
  }); 
});

$('.p-btn').click(function(e){
	if($(window).width()<=991) {
		e.preventDefault();
	  	$(this).next().slideToggle();
	  }
	else {
	}	  	
});


$(window).resize(function(event) {
  if($(window).width() >=991) {
    $('.fa-bars').hide();
  }
  else {
    $('.fa-bars').show();
  }
   if($(window).width() >= 991){
    	$('.nav-bar ul ul').css({
    		display: '',
    	});;
    }

});




$(window).scroll(function(e) {
	if($(window).scrollTop() > 50){
		$('.nav-bar').addClass('fixed');
		$('body').css('margin-top', $('.nav-bar').innerHeight());
	}
	else{
		$('.nav-bar').removeClass('fixed');
		$('body').css('margin-top', '');
	}
});

$(window).scroll(function(e) {
	if($(window).scrollTop() > 300){
		$('.back-to-top').css({
			display: 'block',
		})
	}
	else{
		$('.back-to-top').css ({
			display: 'none',
		})
	}	
});

$('.back-to-top').click(function(e) {
	e.preventDefault();

	 $('html,body').animate({
            scrollTop: 0
        }, 700);

});
